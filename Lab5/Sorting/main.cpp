#include <QCoreApplication>
#include "iostream"


using namespace std;

vector<int> sortVubor(vector<int> array){
    int min = 0;
    int buf = 0;
    int lenght = array.size();

    for (int i = 0; i < lenght; i++)
    {
        min = i;
        for (int j = i + 1; j < lenght; j++)
            min = ( array[j] < array[min] ) ? j : min;
        if (i != min)
        {
            buf = array[i];
            array[i] = array[min];
            array[min] = buf;
        }
    }
    return array;
}

vector<int> sortVstavkami(vector<int> array){
    int lenght = array.size();

    for(int i=1;i<lenght;i++){
        for(int j=i; j>0 && array[j-1]>array[j];j--){
            int tmp=array[j-1];
            array[j-1]=array[j];
            array[j]=tmp;
        }
    }
    return array;
}

QList<int> sortVstavkamiList(QList<int> list){
    int lenght = list.size();
    for(int i=1;i<lenght;i++){
        for(int j=i; j>0 && list[j-1]>list[j];j--){
            int tmp=list[j-1];
            list[j-1]=list[j];
            list[j]=tmp;
        }
    }
    return list;
}

int increment(long inc[], long size) {
    int p1, p2, p3, s;

    p1 = p2 = p3 = 1;
    s = -1;
    do {
        if (++s % 2) {
            inc[s] = 8*p1 - 6*p2 + 1;
        } else {
            inc[s] = 9*p1 - 9*p3 + 1;
            p2 *= 2;
            p3 *= 2;
        }
        p1 *= 2;
    } while(3*inc[s] < size);

    return s > 0 ? --s : 0;
}

vector<int> sortShell(vector<int> array) {
  long inc, i, j, seq[40];
  int s;
  int size = array.size();

  s = increment(seq, size);
  while (s >= 0) {
    inc = seq[s--];

    for (i = inc; i < size; i++) {
      int temp = array[i];
      for (j = i-inc; (j >= 0) && (array[j] > temp); j -= inc)
        array[j+inc] = array[j];
      array[j+inc] = temp;
    }
  }
  return array;
}

void printArray(vector<int> array){
    int lenght = array.size();
    for (int i = 0;i<lenght;i++){
        cout<<array[i]<<" ";
    }
    cout<<endl;
}

void printList(QList<int> list){
    int lenght = list.size();
    for (int i : list){
        cout<<i<<" ";
    }
    cout<<endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    srand(time(0));
    int n = 10000;
    vector <int> array;
    cout<<"Array before sort\n";
    for (int i = 0;i<n;i++){
        array.push_back(rand());
    }
    printArray(array);
    cout<<"Array after vubor sort\n";
    clock_t start = clock();
    vector<int> sortArrayVubor = sortVubor(array);
    clock_t end = clock();
    printArray(sortArrayVubor);
    cout<<"Time: "<<(double)(end - start) / CLOCKS_PER_SEC<<endl;
    cout<<"Array after sort vstavkami\n";
    start = clock();
    vector<int> sortArrayVstavka = sortVstavkami(array);
    end = clock();
    printArray(sortArrayVstavka);
    cout<<"Time: "<<(double)(end - start) / CLOCKS_PER_SEC<<endl;
    QList <int> List;
    for (int i : array){
        List.push_back(i);
    }
    cout<<"List after sort vstavkami\n";
    start = clock();
    QList <int> sortList = sortVstavkamiList(List);
    end = clock();
    printList(sortList);
    cout<<"Time: "<<(double)(end - start) / CLOCKS_PER_SEC<<endl;
    cout<<"Array after sort Shell\n";
    start = clock();
    vector<int> arraySortShell = sortShell(array);
    end = clock();
    printArray(arraySortShell);
    cout<<"Time: "<<(double)(end - start) / CLOCKS_PER_SEC<<endl;
    return a.exec();
}
