#include <QCoreApplication>
#include <iostream>
#include <queue>
#include <stack>
using namespace std;

struct Edge {
    int begin;
    int end;
};


string getName(int i)
{
    switch (i) {
    case 1:
        return  "Kyiv";
    case 2:
        return "Zhytomyr";
    case 3:
        return  "Novograd";
    case 4:
        return  "Rivne";
    case 5:
        return "Lutsk";
    case 6:
        return "Berduchiv";
    case 7:
        return "Vinnucya";
    case 8:
        return "Khmelnutskiy";
    case 9:
        return "Ternopil";
    case 10:
        return "Shepetovka";
    case 11:
        return "Bila cerkva";
    case 12:
        return "Uman";
    case 13:
        return "Cherkasu";
    case 14:
        return "Kremenchug";
    case 15:
        return "Poltava";
    case 16:
        return "Kharkiv";
    case 17:
        return "Priluki";
    case 18:
        return "Sumu";
    case 19:
        return "Mirgorod";
    }
}

vector<int> BFS(int matrix[19][19], int n)
{
    queue<int> Queue;
    vector<int> vershunu;
    int nodes[n];
    for (int i = 0; i < n; i++)
        nodes[i] = 0;
    Queue.push(0);
    while (!Queue.empty())
    {
        int node = Queue.front();
        Queue.pop();
        nodes[node] = 2;
        for (int j = 0; j < n; j++)
        {
            if (matrix[node][j] == 1 && nodes[j] == 0)
            {
                Queue.push(j);
                nodes[j] = 1;
            }
        }
        vershunu.push_back(node+1);
    }
    return vershunu;
}

vector<int> DFS(int matrix[19][19], int n)
{
    stack<int> Stack;
    vector<int> vershunu;
    int nodes[n];
    for (int i = 0; i < n; i++)
        nodes[i] = 0;
    Stack.push(0);
    while (!Stack.empty())
    {
        int node = Stack.top();
        Stack.pop();
        if (nodes[node] == 2) continue;
        nodes[node] = 2;
        for (int j = n-1; j >= 0; j--)
        {
            if (matrix[node][j] == 1 && nodes[j] != 2)
            {
                Stack.push(j);
                nodes[j] = 1;
            }
        }
        vershunu.push_back(node+1);
    }
    return vershunu;
}

void getWay(int matrix[19][19], int n, int req){
    stack<int> Stack;
    int Edge[2];
    stack<struct Edge> Edges;
    struct Edge e;
    int nodes[n];
    for (int i = 0; i < n; i++)
        nodes[i] = 0;
    req--;
    Stack.push(0);
    while (!Stack.empty())
    {
        int node = Stack.top();
        Stack.pop();
        if (nodes[node] == 2) continue;
        nodes[node] = 2;
        for (int j = n-1; j >= 0; j--)
        {
            if (matrix[node][j] == 1 && nodes[j] != 2)
            {
                Stack.push(j);
                nodes[j] = 1;
                e.begin = node; e.end = j;
                Edges.push(e);
                if (node == req) break;
            }
        }
    }
    cout << "Way to " << getName(req+1) << endl;
    cout << getName(req+1);
    while (!Edges.empty())
    {
        e = Edges.top();
        Edges.pop();
        if (e.end == req)
        {
            req = e.begin;
            cout << " <- " << getName(req+1);
        }
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    int n = 19;
    int matrix[19][19] = {
        { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0 }, // матрица смежности
        { 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 }
    };

    int listEdges[18][3] = {
        {1, 2, 135},
        {1, 11, 78},
        {1, 17, 128},
        {2, 3, 80},
        {2, 6, 38},
        {2, 10, 115},
        {3, 4, 100},
        {4, 5, 68},
        {6, 7, 73},
        {7, 8, 110},
        {8, 9, 104},
        {11, 12, 115},
        {11, 13, 146},
        {11, 15, 181},
        {13, 14, 105},
        {15, 16, 130},
        {17, 18, 175},
        {17, 19, 109},
    };

    cout<<"BFS\n";
    foreach (int i, BFS(matrix, n)) {
        cout<<i<<" "<<getName(i)<<endl;
    }
    cout<<endl;
    cout<<"DFS\n";
    foreach (int i, DFS(matrix, n)) {
        cout<<i<<" "<<getName(i)<<endl;
    }
    cout<<endl;
    getWay(matrix, n, 8);
    return a.exec();
}
