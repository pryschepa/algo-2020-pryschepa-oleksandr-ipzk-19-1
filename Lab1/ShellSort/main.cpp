#include <QCoreApplication>

void ShellSort (&array, int size)               // * ∆k = (b∆k−1)/2  ∆0 = N
{
    int step, i, j, tmp;

    // Выбор шага
    for (step = size / 2; step > 0; step /= 2)
    {
        // Перечисление элементов, которые сортируются на определённом шаге
        for (i = step; i < size; i++)
            // Перестановка элементов внутри подсписка, пока i-тый не будет отсортирован
            for (j = i - step; j >= 0 && array[j] > array[j + step]; j -= step)
            {
                tmp = array[j];
                array[j] = array[j + step];
                array[j + step] = tmp;
            }
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    return a.exec();
}
