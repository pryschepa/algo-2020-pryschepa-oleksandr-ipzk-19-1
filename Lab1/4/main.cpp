#include <QCoreApplication>

union float32{
    struct{
        unsigned char low;
        unsigned char high;
    }bytes;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    return a.exec();
}
