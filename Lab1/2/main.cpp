#include <QCoreApplication>
#include <iostream>

using namespace std;

struct Num{
    signed short val : 15;
    signed short sign : 1;
};


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    signed short x;
    cin>> x;
    Num xNum = {x, x};
    cout<<"Value: "<<xNum.val<<"\nSign: "<<xNum.sign;
    return a.exec();
}
