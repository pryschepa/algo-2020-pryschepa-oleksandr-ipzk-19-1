#include <QCoreApplication>
#include <iostream>
#include <bitset>
using namespace std;

void PrintBinary(int var)
{
    if (var<0){
        var = var - var*2;
        cout<<"1 ";
    }
    else{
        cout<<"0 ";
    }
    cout<<bitset<8>(var)<<endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    signed char A = 5 + 127;
    signed char B = 2 - 3;
    signed char C = -120 - 34;
    signed char D = (unsigned char)(-5);
    signed char E = 56 & 38;
    signed char F = 56 | 38;
    cout<<(int)A<<endl;     //cout<<bitset<8>(A-1)<<endl; //инверсия
    PrintBinary(A);
    cout<<(int)B<<endl;
    PrintBinary(B);
    cout<<(int)C<<endl;
    PrintBinary(C);
    cout<<(int)D<<endl;
    PrintBinary(D);
    cout<<(int)E<<endl;
    PrintBinary(E);
    cout<<(int)F<<endl;
    PrintBinary(F);
    return a.exec();
}
