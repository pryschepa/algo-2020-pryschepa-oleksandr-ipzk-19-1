#include <QCoreApplication>
#include <ctime>
#include <iostream>
#include <windows.h>
#include <QTextStream>
#include <QTextCodec>

using namespace std;

struct date{
    unsigned short nMonthDay : 5;
    unsigned short nMonth : 4;
    unsigned short nYear : 11;
    unsigned short nHours : 5;
    unsigned short nMinutes : 6;
    unsigned short nSeconds : 6;
}Date1;

void printDate(date Date){
    cout<<"Date: "<<Date.nMonthDay<<"."<<Date.nMonth<<"."<<Date.nYear<<endl;
    cout<<"Time: "<<Date.nHours<<":"<<Date.nMinutes<<":"<<Date.nSeconds<<endl;
    cout<<"Size: "<<sizeof (Date);
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    time_t now = time(0);
    tm *ltm = localtime(&now);
    Date1.nYear = 1900 + ltm->tm_year;
    Date1.nMonth = 1 + ltm->tm_mon;
    Date1.nMonthDay = ltm->tm_mday;
    Date1.nHours = ltm->tm_hour;
    Date1.nMinutes = ltm->tm_min;
    Date1.nSeconds = ltm->tm_sec;
    printDate(Date1);
    return a.exec();
}
