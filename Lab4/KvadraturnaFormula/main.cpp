#include <QCoreApplication>
#include "math.h"
#include "iostream"

using namespace std;

double GetPow(double x){
    return pow(x,2);
}

int getRandomNumber(int min, int max)
{
     double fraction = 1.0 / (RAND_MAX + 1.0);
     return rand() * fraction * (max - min + 1) + min;
}

double GetResultQuadvature(double a, double b, int segments, double (*f)(double)){
    int n = segments;
    double h = (b-a)/n;
    double Xi[n+1];
    double Qi, result = 0;
    for (int i = 1; i<=n; i++){
        Xi[i] = a + i*h;
        if (i==1){
            Qi = Xi[i] - a;
        }
        else{
            Qi = Xi[i] - Xi[i-1];
        }
        result += Qi*f(Xi[i]);
    }
    return result;
}

double GetResultMonteKarlo(double a, double b, int segments, double (*f)(double)){
    int n = segments;
    double dencity = (b-a)/n;
    double result = 0;
    int Ui[n+1];
    for (int i = 1; i<=n; i++){
        Ui[i] = getRandomNumber(a, b);
        result += f(Ui[i]);
    }
    return dencity * result;
}


int main(int argc, char *argv[])
{
    QCoreApplication Qa(argc, argv);
    srand(time(0));
    double a = 1;
    double b = 3;
    int n = 100000;
    cout<<"Result of quadvature "<<GetResultQuadvature(a, b, n, GetPow)<<endl;
    cout<<"Result of Monte-Karlo "<<GetResultMonteKarlo(a, b, n, GetPow)<<endl;
    return Qa.exec();
}
