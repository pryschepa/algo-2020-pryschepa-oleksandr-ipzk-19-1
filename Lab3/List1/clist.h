#ifndef CLIST_H
#define CLIST_H


#include <stdlib.h>
#include <stdbool.h>

typedef char elemtype;       // Тип элемента списка


struct elem {
    elemtype value;        // Значение переменной
    struct elem* next;      // Ссылка на следующий элемент списка
    struct elem* prev;      // Ссылка на предыдущий элемент списка
};

struct myList {
    struct elem* head;      // Первый элемент списка
    struct elem* tail;      // Последний элемент списка
    size_t size;            // Количество элементов в списке
};

typedef struct elem cNode;
typedef struct myList cList;

cList* createList(void);
void deleteList(cList* list);
bool isEmptyList(cList* list);
elemtype pushFront(cList* list, elemtype* data);
elemtype popFront(cList* list, elemtype* data);
elemtype pushBack(cList* list, elemtype* data);
elemtype popBack(cList* list, elemtype* data);
cNode* getNode(cList* list, int index);
void printList(cList* list, void (*fun)(elemtype*));


#endif // CLIST_H
