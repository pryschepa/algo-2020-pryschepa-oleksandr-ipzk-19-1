#include <QCoreApplication>
#include <stdio.h>
#include <stdlib.h>
#include "clist.h"
#include "iostream"

void printNode(elemtype* value) {
    printf("%d\n", *((char*)value));
}

void printNodeChar(elemtype* value) {
    printf("%c\n", *((char*)value));
}


int main(int argc, char *argv[])
{
    QCoreApplication q(argc, argv);
    elemtype a = 0;
    elemtype b = 1;
    elemtype c = 2;
    elemtype tmp;

    cList* mylist = createList();

    if (isEmptyList(mylist)){
        printf("This list is empty\n");
    }
    else{
        printf("This list isnt empty\n");
    }
    printf("*******\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("*******\n");

    popBack(mylist, &tmp);

    pushFront(mylist, &b);
    printList(mylist, printNode);
    printf("*******\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("*******\n");

    pushBack(mylist, &c);
    printList(mylist, printNode);
    printf("*******\n");

    popFront(mylist, &tmp);
    printList(mylist, printNode);
    printf("*******\n");

    pushBack(mylist, &a);
    printList(mylist, printNode);
    printf("*******\n");

    pushFront(mylist, &a);
    printList(mylist, printNode);
    printf("*******\n");

    printNode(&getNode(mylist, 2)->value);
    printf("*******\n");

    popBack(mylist, &tmp);
    printList(mylist, printNode);
    printf("*******\n");

    if (isEmptyList(mylist)){
        printf("This list is empty\n");
    }
    else{
        printf("This list isnt empty\n");
    }
    printf("*******\n");

    deleteList(mylist);


    system("pause");
    return q.exec();
}
