#include <QCoreApplication>
#include "iostream"
#include "QList"
#include <string.h>
#include <stack>
// Символы во входном потоке следует вводить через пробел.

using namespace std;

int calculate(string str) {
    stack<int> stack_v; //стек
    int n1, n2, res;
    for(int i = 0; i<str.length(); i++) {
        switch(str[i]) {
        case '+':
            n2 = stack_v.top();
            stack_v.pop();
            n1 = stack_v.top();
            stack_v.pop();
            res = n1 + n2;
            stack_v.push(res);
            break;
        case '-':
            n2 = stack_v.top();
            stack_v.pop();
            n1 = stack_v.top();
            stack_v.pop();
            res = n1 - n2;
            stack_v.push(res);
            break;
        case '*':
            n2 = stack_v.top();
            stack_v.pop();
            n1 = stack_v.top();
            stack_v.pop();
            res = n1 * n2;
            stack_v.push(res);
            break;
        case '/':
            n2 = stack_v.top();
            stack_v.pop();
            n1 = stack_v.top();
            stack_v.pop();
            res = n1 / n2;
            stack_v.push(res);
            break;
        case ' ':
            break;
        default: stack_v.push(str[i]-48);
        }
    }
    return stack_v.top();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    string s = "2 5 4 * + 6 - 2 /";
    cout<<calculate(s);
    return a.exec();
}
