#include <QCoreApplication>
#include <math.h>
#include <iostream>
#include <ctime>

using namespace std;

int convert(double value,double From1,double From2,double To1,double To2)
{
      return round((value-From1)/(From2-From1)*(To2-To1)+To1);
}

int main(int argc, char *argv[])
{
    QCoreApplication apl(argc, argv);
    int lenght = 4000;
    long randomNum [lenght];
    int randomNumConvert[lenght];
    int a = 2147483629;
    int c = 2147483587;
    int max = INT_MIN;
    int min = INT_MAX;
    int m = pow(2,31)-1;
    randomNum[0] = time(NULL);
    for (int i = 1; i<lenght; i++){
        randomNum[i] = (a*randomNum[i-1]+c)%m;
        //cout<<randomNum[i-1]<<endl;
    }
    //cout<<randomNum[lenght-1]<<endl;

    for (int i=0;i<lenght;i++){
        if (randomNum[i]>max){
            max = randomNum[i];
        }
        if(randomNum[i]<min){
            min = randomNum[i];
        }
    }

    for (int i=0;i<lenght;i++){
        randomNumConvert[i]=convert(randomNum[i], min, max, 0, 150);
        cout<<randomNumConvert[i]<<endl;
    }

    int repeat=0;
    double avg = 0;
    double chance[151];
    for(int i=0; i<151; i++)
    {
        for(int j=0; j<lenght; j++)
        {
            if(i == randomNumConvert[j]) repeat++;
        }
        chance[i]=(double)repeat/lenght;
        cout <<i<<" repeat "<<repeat<<" times"<<endl;
        cout<<"Chance: "<<chance[i]<<endl;
        avg+=chance[i]*i;
        repeat = 0;
    }
    cout<<"Average value: "<<avg<<endl;

    double disp = 0, dev;
    for (int i =0;i<151;i++){
        disp+=pow((randomNumConvert[i]-avg),2)*chance[i];
    }
    cout<<"Dispersion: "<<disp<<endl;
    dev = sqrt(disp);
    cout<<"Standard deviation: "<<dev<<endl;
    return apl.exec();
}
